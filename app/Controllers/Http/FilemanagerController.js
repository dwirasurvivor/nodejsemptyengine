'use strict'

const Database = use('Database')
const Env = use('Env')
const fs = require('fs')
const paths = require('path')
const multer = require('multer')
paths.posix = require('path-posix')
const upload = multer({dest: 'public/uploads/'})
const Logger = use('Logger')
const Helpers = use('Helpers')
const oss = require('ali-oss')
const co = require('co')
const sharp = require('sharp')
const dir = require('node-dir')
const moment = require('moment')
const oquery = require('./Helper/ObjectQuery.js')

class FilemanagerController {
	async index({request, response, auth, view}) {
		let req = request.get()
		let data = {
			exclusive: req.exclusive,
			input: req.input
		}
		return view.render('filemanager.index', { param: data })
	}
	
	async getData({request, response, auth, view}) {
		let req = request.all()
		let limit = parseInt(req.limit)
		let fpage = parseInt(req.page) - 1
		let page = fpage * limit
		let sort = parseInt(req.sort) || 1
		let search = req.search || ''
		let dirpath
		if (req.exclusive) {
			dirpath = 'public' + req.exclusive
		} else {
			dirpath = 'public' + req.path
		}
		
		let data, datafiles, prevpath
		let dirinfo = []
		let filewithinfo = []
		let exludedir = ['prescriptions', 'mediums', 'thumbs']
		
		let dirs = dir.files(dirpath, 'dir', null, { sync: true, recursive: false })
		let files = dir.files(dirpath, 'file', null, { sync: true, recursive: false })
		
		for(let x in dirs) {
			let dirsreplace = dirs[x].replace(/\\/g, '/')
			let dirsplit = dirsreplace.split('/')
			let lastdirs = dirsplit[dirsplit.length - 1]
			let inarray = exludedir.includes(lastdirs)
			if (!inarray) {
				dirinfo.push({
					name: dirsplit[dirsplit.length - 1],
					path: dirs[x].replace(/\\/g, '/').replace(/public\//g, '/')
				})
			}
		}
		
		for(let x in files) {
			let stats = fs.statSync(files[x])
			let filereplace = files[x].replace(/\\/g, '/')
			let filesplit = filereplace.split('/')
			let newfilename = filesplit[filesplit.length - 1].replace(/_/g, '-').replace(/-/g, ' ').toLowerCase()
			let statswithname = {
				name: newfilename.substr(0, 50),
				oriname: filesplit[filesplit.length - 1],
				ext: this.fileExtension(filesplit[filesplit.length - 1]),
				path: files[x].replace(/\\/g, '/').replace(/public\//g, '/'),
				size: this.bytesToSize(stats['size']),
				orisize: parseFloat(stats['size'].toFixed(0)),
				birthtime: moment(stats['birthtime']).format('YYYY-MM-DD HH:mm A'),
				microtime: parseFloat(stats['birthtimeMs'].toFixed(0))
			}
			filewithinfo.push(statswithname)
		}
		
		if (sort == 1) {
			if (search == '') {
				datafiles = await oquery().sort('name').asc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('name').asc().limit(limit).offset(page).on(filewithinfo)
			}
		} else if (sort == 2) {
			if (search == '') {
				datafiles = await oquery().sort('name').desc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('name').desc().limit(limit).offset(page).on(filewithinfo)
			}
		} else if (sort == 3) {
			if (search == '') {
				datafiles = await oquery().sort('microtime').asc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('microtime').asc().limit(limit).offset(page).on(filewithinfo)
			}
		} else if (sort == 4) {
			if (search == '') {
				datafiles = await oquery().sort('microtime').desc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('microtime').desc().limit(limit).offset(page).on(filewithinfo)
			}
		} else if (sort == 5) {
			if (search == '') {
				datafiles = await oquery().sort('orisize').asc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('orisize').asc().limit(limit).offset(page).on(filewithinfo)
			}
		} else {
			if (search == '') {
				datafiles = await oquery().sort('orisize').desc().limit(limit).offset(page).on(filewithinfo)
			} else {
				datafiles = await oquery('name').search(search).sort('orisize').desc().limit(limit).offset(page).on(filewithinfo)
			}
		}
		
		let oripath = dirpath.replace(/\\/g, '/').replace(/public\//g, '/')
		let orinewpath = oripath.split('/')
		orinewpath.pop()
		
		response.header('Content-type', 'application/json')
		response.type('application/json')
		data = {
			data: {
				dirs: dirinfo,
				files: datafiles,
			},
			path: dirpath.replace(/\\/g, '/').replace(/public\//g, '/'),
			prevpath: orinewpath.join('/') + '/',
			total: filewithinfo.length,
			totalpage: Math.ceil(filewithinfo.length / limit),
			page: fpage + 1,
			limit: limit,
			search: search
		}
		return response.send(data)
	}
	
	async upload({request, response, auth, view}) {
		let req = request.all()
		let path = req.path.replace('public/', '')
		let data
		
		const fileup = request.file('files', {
			size: '100mb'
		})
		
		await fileup.moveAll(Helpers.publicPath(path), (file) => {
			let oriname = file.clientName
			let orinewname = oriname.split('.')
			let oriext = this.getFileExtension(oriname)
			let uniqkey = Math.floor(Math.random() * 99)
			orinewname.pop()
			let newfilename = orinewname.join('-').toLowerCase().replace(' ', '-').replace('_', '-') + '-' + uniqkey + '.' + oriext
			return {
				name: newfilename.replace(' ', '-').replace('_', '-')
			}
		})

		if (!fileup.movedAll()) {
			response.header('Content-type', 'application/json')
			response.type('application/json')
			data = {
				success: 400,
				message: []
			}
			return response.send(data)
		} else {
			let movedFiles = fileup.movedList()
			for(let x in movedFiles) {
				if (path == 'uploads/images/' || path == '/uploads/images/') {
					if (movedFiles[x]['type'] == 'image') {
						this.imageResize(path, movedFiles[x]['fileName'], 'mediums', 500)
						this.imageResize(path, movedFiles[x]['fileName'], 'thumbs', 200)
					}
				}
			}
			
			response.header('Content-type', 'application/json')
			response.type('application/json')
			data = {
				error: 200,
				message: []
			}
			return response.send(data)
		}
	}
	
	async delete({request, response, auth, view}) {
		let req = request.all()
		let file = Helpers.publicPath(req.file)
		let data
		
		let filenamedel = req.file.split("/").pop()
		let filenamedelmed = 'mediums/' + filenamedel
		let filenamedelthm = 'thumbs/' + filenamedel
		
		if (req.file == '/uploads/images/' + filenamedel) {
			fs.unlinkSync('public/uploads/images/mediums/' + filenamedel)
			fs.unlinkSync('public/uploads/images/thumbs/' + filenamedel)
		}
		
		fs.unlinkSync(file)
		
		response.header('Content-type', 'application/json')
		response.type('application/json')
		data = {
			error: 200,
			message: []
		}
		return response.send(data)
	}
	
	imageResize(path, name, type, size) {
		sharp(Helpers.publicPath(path) + '/' + name)
			.resize(size, size)
			.toFile(Helpers.publicPath(path) + '/' + type + '/' + name)
			.then(info => {
				this.uploadResize(path, name, type)
			})
			.catch(err => {})
		sharp.cache(false)
		
		return true
	}
	
	uploadResize(path, name, type) {
		return true
	}
	
	getFileExtension(filename) {
		return filename.split('.').pop()
	}
	
	bytesToSize(bytes) {
		const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
		if (bytes === 0) return 'n/a'
		const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
		if (i === 0) return `${bytes} ${sizes[i]}`
		return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
	}
	
	fileExtension(filename, opts) {
		if (!opts) opts = {}
		if (!filename) return ""
		var ext = (/[^./\\]*$/.exec(filename) || [""])[0]
		return opts.preserveCase ? ext : ext.toLowerCase()
	}
}

module.exports = FilemanagerController