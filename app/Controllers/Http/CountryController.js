'use strict'

const Country = use('App/Models/Country')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class CountryController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Country', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('country.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'countries',
			sSelectSql: ['id', 'country_code', 'country_name'],
			aSearchColumns: ['country_code', 'country_name']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['country_code'],
				select[0][x]['country_name'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./country/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('country.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { country_code, country_name } = request.only(['country_code', 'country_name'])
		
		let formData = {
			country_code: country_code,
			country_name: country_name,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			country_code: 'required',
			country_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Country.create(formData)
			session.flash({ notification: 'Country added', status: 'aquamarine' })
			return response.redirect('/country')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let country = await Country.find(params.id)

		if (country) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('country.edit', {
				meta: meta,
				country: country
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Country not found', status: 'danger' })
			return response.redirect('/country')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { country_code, country_name } = request.only(['country_code', 'country_name'])
		
		let country = await Country.find(params.id)
		
		let formData = {
			country_code: country_code,
			country_name: country_name,
			updated_by: auth.user.id
		}
		
		let rules = {
			country_code: 'required',
			country_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (country) {
				await Country.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Country updated', status: 'aquamarine' })
				return response.redirect('/country')
			} else {
				session.flash({ notification: 'Country not found', status: 'danger' })
				return response.redirect('/country')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let country = await Country.find(formData.id)
		if (country){
			try {
				await country.delete()
				session.flash({ notification: 'Country success deleted', status: 'aquamarine' })
				return response.redirect('/country')
			} catch (e) {
				session.flash({ notification: 'Country cannot be delete', status: 'danger' })
				return response.redirect('/country')
			}
		} else {
			session.flash({ notification: 'Country cannot be delete', status: 'danger' })
			return response.redirect('/country')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Country', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let country = await Country.find(formData.item[i])
				try {
					await country.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Country success deleted', status: 'aquamarine' })
			return response.redirect('/country')
		} else {
			session.flash({ notification: 'Country cannot be deleted', status: 'danger' })
			return response.redirect('/country')
		}
	}

	async getCountry({request, response, auth, view, session}) {
		let req = request.get()
		let countryforms
		if (req.phrase != 'undefined') {
			countryforms = await Country.query().where('country_name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			countryforms = await Country.query().limit(20).fetch()
		}
		let countryform = countryforms.toJSON()
		
		let data = []
		for(let i in countryform){
			data.push({
				id: countryform[i]['id'],
				text: countryform[i]['country_name']
			})
		}		
		return JSON.stringify({ results: data })
	}

	async getCountryList({request, response, auth, view, session}) {
		let countryforms = await Country.query().select('id','country_name as name').orderBy('country_name', 'asc').fetch()
		let countries = countryforms.toJSON()
		
		return response.send(countries)
	}
}

module.exports = CountryController