'use strict'

const Regency = use('App/Models/Regency')
const Province = use('App/Models/Province')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class RegencyController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Regency', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('regency.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'regencies',
			sSelectSql: ['id', 'province_id', '(select province_name from provinces where province_id = id) as province_name', 'regency_name', 'regency_iso', 'regency_hasc', 'regency_fips'],
			aSearchColumns: ['regency_name']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['province_name'],
				select[0][x]['regency_name'],
				select[0][x]['regency_iso'],
				select[0][x]['regency_hasc'],
				select[0][x]['regency_fips'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./regency/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let provinces = await Province.all()
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('regency.create', {
			meta: meta,
			provinces: provinces.toJSON()
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { province_id, regency_name, regency_iso, regency_hasc, regency_fips } = request.only(['province_id', 'regency_name', 'regency_iso', 'regency_hasc', 'regency_fips'])
		
		let formData = {
			province_id: province_id,
			regency_name: regency_name,
			regency_iso: regency_iso,
			regency_hasc: regency_hasc,
			regency_fips: regency_fips,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			province_id: 'required',
			regency_name: 'required',
			regency_iso: 'required',
			regency_hasc: 'required',
			regency_fips: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Regency.create(formData)
			session.flash({ notification: 'Regency added', status: 'aquamarine' })
			return response.redirect('/regency')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let regency = await Regency.find(params.id)
		let provinces = await Province.all()

		if (regency) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('regency.edit', {
				meta: meta,
				regency: regency,
				provinces: provinces.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Regency not found', status: 'danger' })
			return response.redirect('/regency')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { province_id, regency_name, regency_iso, regency_hasc, regency_fips } = request.only(['province_id', 'regency_name', 'regency_iso', 'regency_hasc', 'regency_fips'])
		
		let regency = await Regency.find(params.id)
		
		let formData = {
			province_id: province_id,
			regency_name: regency_name,
			regency_iso: regency_iso,
			regency_hasc: regency_hasc,
			regency_fips: regency_fips,
			updated_by: auth.user.id
		}
		
		let rules = {
			province_id: 'required',
			regency_name: 'required',
			regency_iso: 'required',
			regency_hasc: 'required',
			regency_fips: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (regency) {
				await Regency.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Regency updated', status: 'aquamarine' })
				return response.redirect('/regency')
			} else {
				session.flash({ notification: 'Regency not found', status: 'danger' })
				return response.redirect('/regency')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let regency = await Regency.find(formData.id)
		if (regency){
			try {
				await regency.delete()
				session.flash({ notification: 'Regency success deleted', status: 'aquamarine' })
				return response.redirect('/regency')
			} catch (e) {
				session.flash({ notification: 'Regency cannot be delete', status: 'danger' })
				return response.redirect('/regency')
			}
		} else {
			session.flash({ notification: 'Regency cannot be delete', status: 'danger' })
			return response.redirect('/regency')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let regency = await Regency.find(formData.item[i])
				try {
					await regency.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Regency success deleted', status: 'aquamarine' })
			return response.redirect('/regency')
		} else {
			session.flash({ notification: 'Regency cannot be deleted', status: 'danger' })
			return response.redirect('/regency')
		}
	}

	async getbyprovince({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Regency', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let regency = await Regency.query().select('id','regency_name as name').where('province_id',params.id).orderBy('regency_name', 'asc');
		return regency
	}

	async getRegency({request, response, auth, view, session}) {
		let req = request.get()
		let regencies
		if (req.phrase != 'undefined') {
			regencies = await Regency.query().select('id', 'regency_name').where('regency_name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			regencies = await Regency.query().select('id', 'regency_name').limit(20).fetch()
		}
		let regency = regencies.toJSON()
		
		let data = []
		for(let i in regency){
			data.push({
				id: regency[i]['id'],
				text: regency[i]['regency_name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = RegencyController