'use strict'

const Province = use('App/Models/Province')
const Country = use('App/Models/Country')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class ProvinceController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Province', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('province.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'provinces',
			sSelectSql: ['id', 'province_code', 'province_name', 'country_id', '(select country_name from countries where country_id = id) as country_name', 'province_iso', 'province_hasc', 'province_fips'],
			aSearchColumns: ['province_code', 'province_name', 'province_iso', 'province_hasc', 'province_fips']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['province_code'],
				select[0][x]['province_name'],
				select[0][x]['country_name'],
				select[0][x]['province_iso'],
				select[0][x]['province_hasc'],
				select[0][x]['province_fips'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./province/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let countries = await Country.all()
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('province.create', {
			meta: meta,
			countries: countries.toJSON()
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { province_code, province_name, country_id, province_iso, province_hasc, province_fips } = request.only(['province_code', 'province_name', 'country_id', 'province_iso', 'province_hasc', 'province_fips'])
		
		let formData = {
			province_code: province_code,
			province_name: province_name,
			country_id: country_id,
			province_iso: province_iso,
			province_hasc: province_hasc,
			province_fips: province_fips,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			country_id: 'required',
			province_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Province.create(formData)
			session.flash({ notification: 'Province added', status: 'aquamarine' })
			return response.redirect('/province')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let province = await Province.find(params.id)
		let countries = await Country.all()

		if (province) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('province.edit', {
				meta: meta,
				province: province,
				countries: countries.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Province not found', status: 'aquamarine' })
			return response.redirect('/province')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { province_code, province_name, country_id, province_iso, province_hasc, province_fips } = request.only(['province_code', 'province_name', 'country_id', 'province_iso', 'province_hasc', 'province_fips'])
		
		let province = await Province.find(params.id)
		
		let formData = {
			province_code: province_code,
			province_name: province_name,
			country_id: country_id,
			province_iso: province_iso,
			province_hasc: province_hasc,
			province_fips: province_fips,
			updated_by: auth.user.id
		}
		
		let rules = {
			country_id: 'required',
			province_name: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (province) {
				await Province.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Province updated', status: 'aquamarine' })
				return response.redirect('/province')
			} else {
				session.flash({ notification: 'Province not found', status: 'danger' })
				return response.redirect('/province')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let province = await Province.find(formData.id)
		if (province){
			try {
				await province.delete()
				session.flash({ notification: 'Province success deleted', status: 'aquamarine' })
				return response.redirect('/province')
			} catch (e) {
				session.flash({ notification: 'Province cannot be delete', status: 'danger' })
				return response.redirect('/province')
			}
		} else {
			session.flash({ notification: 'Province cannot be delete', status: 'danger' })
			return response.redirect('/province')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Province', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let province = await Province.find(formData.item[i])
				try {
					await province.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Province success deleted', status: 'aquamarine' })
			return response.redirect('/province')
		} else {
			session.flash({ notification: 'Province cannot be deleted', status: 'danger' })
			return response.redirect('/province')
		}
	}

	async getProvince({request, response, auth, view, session}) {
		let req = request.get()
		let provinces
		if (req.phrase != 'undefined') {
			provinces = await Province.query().select('id', 'province_name').where('province_name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			provinces = await Province.query().select('id', 'province_name').limit(20).fetch()
		}
		let province = provinces.toJSON()
		
		let data = []
		for(let i in province){
			data.push({
				id: province[i]['id'],
				text: province[i]['province_name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async getByCountry({params, request, response, auth, view, session}) {
		let provinces = await Province.query().select('id', 'province_name as name').where('country_id', params.id).orderBy('province_name','asc').fetch()
		let province = provinces.toJSON()
		
		return response.send(province)
	}
}

module.exports = ProvinceController