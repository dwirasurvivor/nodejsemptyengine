'use strict'

const { Command } = use('@adonisjs/ace')
const shell = require('shelljs')
const glob = require('glob')
const fs = require('fs')
const replacer = require('replace')
const Helpers = use('Helpers')
const camelCase = require('uppercamelcase')
const pluralize = require('pluralize')
const _lo = require('lodash')
const Logger = use('Logger')

class GeneratorController {
	async index({request, response, auth, view}) {
		return view.render('generator.index')
	}

	async generated({request, response, auth, view, session}) {
		const formData = request.post()
		if (formData) {
			let crud_name = formData.crud_name
			let fieldsArray = []
			let validationsArray = []
			let x = 0
			for (let i in formData['fields']) {
				if (formData['fields_required'][i] == '1') {
					validationsArray.push(formData['fields'][i])
				}
				fieldsArray.push(formData['fields'][i] + '#' + formData['fields_type'][i] + '#' + formData['fields_index'][i])
				x++
			}
			let fields = fieldsArray.join(';')
			let validations = validationsArray.join('#required;') + '#required'
			Logger.info(fields)
			await this.createFile(crud_name, fields, validations)
        }
		session.flash({ notification: 'Module created successfuly' })
		response.route('GeneratorController.index', {})
	}
	
	async createFile(crud_name, fields, validations) {
		let get_migration = shell.exec('adonis make:migration ' + crud_name + ' --action=create', {silent:true}).stdout
		let get_migration_filter = get_migration.replace(' ', '').replace('\n', '').replace(/\\/g, '/').split('database/migrations/')
		let get_migration_name = get_migration_filter[1].replace(/\r?\n|\r/g, '')
		await this.firstFile('/database/migrations/', get_migration_name, 'migration')
		await this.replaceMigrationFile(get_migration_name, crud_name, fields, validations)
	}
	
	async replaceMigrationFile(get_migration_name, crud_name, fields, validations) {
		await this.replaceFile('/database/migrations/', get_migration_name, '{{schema_name}}', await this.jsUcfirst(pluralize(camelCase(crud_name))) + 'Schema')
		await this.replaceFile('/database/migrations/', get_migration_name, '{{schema_table}}', pluralize(crud_name).toLowerCase())
		let flds = fields.split(';')
		let schemaFields = ''
		let schemaIndexs = ''
		let tabIndent = '			'
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
			schemaFields += 'table.' + fieldArray[1] + '(\'' + fieldArray[0] + '\')\n'
			schemaFields += tabIndent
			if (fieldArray[2] == '1') {
				schemaIndexs += 'this.raw(\'ALTER TABLE `' + pluralize(crud_name).toLowerCase() + '` ADD INDEX `' + fieldArray[0] + '` (`' + fieldArray[0] + '`)\')\n'
			}
			schemaIndexs += tabIndent
        }
		await this.replaceFile('/database/migrations/', get_migration_name, '{{schema_up}}', schemaFields)
		await this.replaceFile('/database/migrations/', get_migration_name, '{{schema_index}}', schemaIndexs)
		
		shell.exec('adonis make:model ' + crud_name)
		await this.firstFile('/app/Models/', camelCase(crud_name) + '.js', 'model')
		await this.replaceModelFile(crud_name, fields, validations)
	}
	
	async replaceModelFile(crud_name, fields, validations) {
		await this.replaceFile('/app/Models/', camelCase(crud_name) + '.js', '{{model_name}}', await this.jsUcfirst(camelCase(crud_name)))
		shell.exec('adonis make:controller ' + crud_name + ' --type=http')
		await this.firstFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', 'controller')
		await this.replaceControllerFile(crud_name, fields, validations)
	}
	
	async replaceControllerFile(crud_name, fields, validations) {
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{controller_name}}', await this.jsUcfirst(pluralize(camelCase(crud_name))) + 'Controller')
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{model_name}}', await this.jsUcfirst(camelCase(crud_name)))
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{plural_name}}', pluralize(camelCase(crud_name)).toLowerCase())
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{non_plural_name}}', camelCase(crud_name).toLowerCase())
		let vlds = validations.split(';')
		let flds = fields.split(';')
		let exFields = ''
		let validationFields = ''
		let whereFields = ''
		let arrFields = ''
		let arrFieldTables = ''
		let insFields = ''
		let insToFields = ''
		let ruleToFields = ''
		let editRuleToFields = ''
		let tabIndent = '			'
		let tabIndent2 = '			'
		let tabIndent3 = '					'
		let tabIntent4 = '				'
		let tabIntent5 = '			'
		for (let i in vlds) {
			let fieldArray = vlds[i].split('#')
			validationFields += fieldArray[0] + ': ' + '\'' + fieldArray[1] + '\',\n'
			validationFields += tabIndent
			ruleToFields += fieldArray[0] + ': \'' + fieldArray[1] + '\',\n'
			ruleToFields += tabIntent5
			editRuleToFields += fieldArray[0] + ': \'' + fieldArray[1] + '\',\n'
			editRuleToFields += tabIntent5
        }
		let fieldArrayWhere = vlds[0].split('#')
		whereFields += '.where(\'' + fieldArrayWhere[0] + '\', \'LIKE\', \'%\' + formData.query + \'%\')'
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
            exFields += fieldArray[0] + ', '
			arrFields += '\'' + fieldArray[0] + '\', '
			arrFieldTables += 'select[0][x][\'' + fieldArray[0] + '\'],\n'
			arrFieldTables += tabIntent4
			insFields += fieldArray[0] + ': formData.' + fieldArray[0] + ',\n'
			insFields += tabIndent2
			insToFields += fieldArray[0] + ': ' + fieldArray[0] + ',\n'
			insToFields += tabIntent5
        }
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{fields}}', exFields.slice(0, -2))
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{where_fields}}', whereFields)
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{arr_fields}}', arrFields.slice(0, -2))
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{arr_field_tables}}', arrFieldTables.slice(0, -2))
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{valid_fields}}', validationFields)
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{insert_fields}}', insFields)
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{insert_to_fields}}', insToFields)
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{rule_to_fields}}', ruleToFields)
		await this.replaceFile('/app/Controllers/Http/', camelCase(crud_name) + 'Controller.js', '{{edit_rule_to_fields}}', editRuleToFields)
		
		let cmd = new Command()
		await cmd.ensureDir(Helpers.appRoot('resources/views/' + camelCase(crud_name).toLowerCase()))
		await cmd.ensureFile(Helpers.appRoot('resources/views/' + camelCase(crud_name).toLowerCase() + '/index.edge'))
		await cmd.ensureFile(Helpers.appRoot('resources/views/' + camelCase(crud_name).toLowerCase() + '/create.edge'))
		await cmd.ensureFile(Helpers.appRoot('resources/views/' + camelCase(crud_name).toLowerCase() + '/edit.edge'))
		
		await this.firstFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', 'index')
		await this.replaceIndexFile(crud_name, fields, validations)
	}
	
	async replaceIndexFile(crud_name, fields, validations) {
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{controller_name}}', await this.jsUcfirst(pluralize(camelCase(crud_name))))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{model_name}}', await this.jsUcfirst(camelCase(crud_name)))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{plural_name}}', pluralize(camelCase(crud_name)).toLowerCase())
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{non_plural_name}}', camelCase(crud_name).toLowerCase())
		let vlds = validations.split(';')
		let flds = fields.split(';')
		let validationFields = ''
		let whereFields = ''
		let arrFields = ''
		let columnFields = ''
		let countFields = parseInt(flds.length) + 2
		let tabIndent1 = '										'
		let tabIndent2 = '            '
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
			columnFields += '<th>' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '</th>\n'
			columnFields += tabIndent1
        }
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
			arrFields += '{ "name" : "' + fieldArray[0] + '" },\n'
			arrFields += tabIndent2
        }
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{column_fields}}', columnFields.slice(0, -2))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{arr_fields}}', arrFields.slice(0, -2))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'index.edge', '{{count_fields}}', countFields)
		
		await this.firstFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', 'create')
		await this.replaceCreateFile(crud_name, fields, validations)
	}
	
	async replaceCreateFile(crud_name, fields, validations) {
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{controller_name}}', await this.jsUcfirst(pluralize(camelCase(crud_name))))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{model_name}}', await this.jsUcfirst(camelCase(crud_name)))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{plural_name}}', pluralize(camelCase(crud_name)).toLowerCase())
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{non_plural_name}}', camelCase(crud_name).toLowerCase())
		let vlds = validations.split(';')
		let flds = fields.split(';')
		let validationFields = ''
		let arrFields = ''
		let arrDataFields = ''
		let arrClearFields = ''
		let formFields = ''
		let formValidates = ''
		let formValidateMessages = ''
		let tabIndent = '			'
		let tabIndent2 = '						'
		for (let i in vlds) {
			let fieldArray = vlds[i].split('#')
			validationFields += 'this.' + fieldArray[0] + ' == \'\' || '
			formValidates += '				' + fieldArray[0] + ': {\n'
			formValidates += '					' + fieldArray[1] + ': true\n'
			formValidates += '				},\n'
			formValidateMessages += '				' + fieldArray[0] + ': {\n'
			formValidateMessages += '					' + fieldArray[1] + ': "Please enter a ' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '"\n'
			formValidateMessages += '				},\n'
        }
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
			arrFields += fieldArray[0] + ': \'\',\n'
			arrFields += tabIndent
			arrDataFields += fieldArray[0] + ': this.' + fieldArray[0] + ',\n'
			arrDataFields += tabIndent2
			arrClearFields += 'this.' + fieldArray[0] + ' = \'\'\n'
			arrClearFields += tabIndent2
			formFields += '						<div class="form-group">\n'
			formFields += '							<label class="form-label" for="' + fieldArray[0] + '">' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '</label>\n'
			formFields += '							<input type="text" class="form-control" name="' + fieldArray[0] + '" id="' + fieldArray[0] + '" value="{{ old(\'' + fieldArray[0] + '\', \'\') }}" placeholder="' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '" />\n'
			formFields += '							{{ elIf(\'<span class="help-block text-danger">$self</span>\', getErrorFor(\'' + fieldArray[0] + '\'), hasErrorFor(\'' + fieldArray[0] + '\')) }}\n'
			formFields += '						</div>\n'
        }
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{valid_fields}}', validationFields.slice(0, -4))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_fields}}', arrFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_data_fields}}', arrDataFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_clear_fields}}', arrClearFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_form_fields}}', formFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_form_validates}}', formValidates)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'create.edge', '{{arr_form_validate_messages}}', formValidateMessages)
		
		await this.firstFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', 'edit')
		await this.replaceEditFile(crud_name, fields, validations)
	}
	
	async replaceEditFile(crud_name, fields, validations) {
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{controller_name}}', await this.jsUcfirst(pluralize(camelCase(crud_name))))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{model_name}}', await this.jsUcfirst(camelCase(crud_name)))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{plural_name}}', pluralize(camelCase(crud_name)).toLowerCase())
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{non_plural_name}}', camelCase(crud_name).toLowerCase())
		let vlds = validations.split(';')
		let flds = fields.split(';')
		let validationFields = ''
		let arrFields = ''
		let arrDataFields = ''
		let arrClearFields = ''
		let arrAsyncFields = ''
		let formFields = ''
		let formValidates = ''
		let formValidateMessages = ''
		let tabIndent = '			'
		let tabIndent2 = '						'
		let tabIndent3 = '				'
		for (let i in vlds) {
			let fieldArray = vlds[i].split('#')
			validationFields += 'this.' + fieldArray[0] + ' == \'\' || '
			formValidates += '				' + fieldArray[0] + ': {\n'
			formValidates += '					' + fieldArray[1] + ': true\n'
			formValidates += '				},\n'
			formValidateMessages += '				' + fieldArray[0] + ': {\n'
			formValidateMessages += '					' + fieldArray[1] + ': "Please enter a ' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '"\n'
			formValidateMessages += '				},\n'
        }
		for (let i in flds) {
			let fieldArray = flds[i].split('#')
			arrFields += fieldArray[0] + ': \'\',\n'
			arrFields += tabIndent
			arrDataFields += fieldArray[0] + ': this.' + fieldArray[0] + ',\n'
			arrDataFields += tabIndent2
			arrClearFields += 'this.' + fieldArray[0] + ' = \'\'\n'
			arrClearFields += tabIndent2
			formFields += '						<div class="form-group">\n'
			formFields += '							<label class="form-label" for="' + fieldArray[0] + '">' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '</label>\n'
			formFields += '							<input type="text" class="form-control" name="' + fieldArray[0] + '" id="' + fieldArray[0] + '" value="{{ old(\'' + fieldArray[0] + '\', ' + camelCase(crud_name).toLowerCase() +'.' + fieldArray[0] + ' || \'\') }}" placeholder="' + await this.toTitleCase(fieldArray[0].replace('_', ' ')) + '" />\n'
			formFields += '							{{ elIf(\'<span class="help-block text-danger">$self</span>\', getErrorFor(\'' + fieldArray[0] + '\'), hasErrorFor(\'' + fieldArray[0] + '\')) }}\n'
			formFields += '						</div>\n'
			arrAsyncFields += fieldArray[0] + ': res.data.data.' + camelCase(crud_name).toLowerCase() + '.' + fieldArray[0] + ',\n'
			arrAsyncFields += tabIndent3
        }
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{valid_fields}}', validationFields.slice(0, -4))
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_fields}}', arrFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_data_fields}}', arrDataFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_clear_fields}}', arrClearFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_form_fields}}', formFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_async_fields}}', arrAsyncFields)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_form_validates}}', formValidates)
		await this.replaceFile('/resources/views/' + camelCase(crud_name).toLowerCase() + '/', 'edit.edge', '{{arr_form_validate_messages}}', formValidateMessages)
		
		let addRouter = ''
		addRouter += '	Route.post(\'' + camelCase(crud_name).toLowerCase() + '/datatable\', \'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller.datatable\')\n'
		addRouter += '	Route.post(\'' + camelCase(crud_name).toLowerCase() + '/delete\', \'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller.delete\')\n'
		addRouter += '	Route.post(\'' + camelCase(crud_name).toLowerCase() + '/multidelete\', \'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller.multidelete\')\n'
		addRouter += '	Route.resource(\'' + camelCase(crud_name).toLowerCase() + '\', \'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller\')\n\n'
		addRouter += '// --------------------------'
		await this.replaceFile('/start/', 'routes.js', '// --------------------------', addRouter)
		
		let addMenu = ''
		addMenu += '			<li class="grey with-sub">\n'
		addMenu += '				<span><i class="font-icon font-icon-page"></i><span class="lbl">' + await this.jsUcfirst(pluralize(camelCase(crud_name))) + '</span></span>\n'
		addMenu += '				<ul>\n'
		addMenu += '					<li><a href="{{ route(\'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller.index\') }}"><span class="lbl">List ' + await this.jsUcfirst(pluralize(camelCase(crud_name))) + '</span></a></li>\n'
		addMenu += '					<li><a href="{{ route(\'' + await this.jsUcfirst(camelCase(crud_name)) + 'Controller.create\') }}"><span class="lbl">Add ' + await this.jsUcfirst(pluralize(camelCase(crud_name))) + '</span></a></li>\n'
		addMenu += '				</ul>\n'
		addMenu += '			</li>\n\n'
		addMenu += '<!-- -------------------------- !-->'
		await this.replaceFile('/resources/views/layouts/', 'admin.edge', '<!-- -------------------------- !-->', addMenu)
		
		shell.exec('adonis migration:run -f')
	}
	
	async firstFile(path, filename, template) {
		await glob(Helpers.appRoot() + '/resources/views/generator/templates/' + template +'.tmpl', function(err, files) {
			if (err) { throw err; }
			fs.writeFileSync(Helpers.appRoot() + path + filename, '')
			files.forEach(function(item, index, array) {
				fs.writeFileSync(Helpers.appRoot() + path + filename, fs.readFileSync(item, 'utf8'))
			})
		})
	}
	
	async replaceFile(path, filename, str, rep) {
		await glob(Helpers.appRoot() + path + filename, function(err, files) {
			if (err) { throw err; }
			files.forEach(function(item, index, array) {
				replacer({
					regex: str,
					replacement: rep,
					paths: [item],
					recursive: true,
					silent: true
				})
			})
		});
	}
	
	async jsUcfirst(string)  {
		return string.charAt(0).toUpperCase() + string.slice(1)
	}
	
	async toTitleCase(str) {
		return str.replace(/\w\S*/g, function(txt){
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
		})
	}
}

module.exports = GeneratorController