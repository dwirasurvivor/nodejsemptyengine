'use strict'

const User = use('App/Models/User')
const Role = use('App/Models/Role')
const RoleUser = use('App/Models/RoleUser')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class UserController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('User', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('users.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition
		
		if (auth.user.id == '1') {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["users.id", "users.username", "users.email", "users.fullname"],
				aSearchColumns: ["users.username", "users.email", "users.fullname"]
			}
		} else {
			tableDefinition = {
				sTableName: 'users',
				sSelectSql: ["id", "username", "email", "fullname"],
				aSearchColumns: ["username", "email", "fullname"],
				sWhereAndSql: 'id = "'+ auth.user.id +'"'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['username'],
				select[0][x]['fullname'],
				select[0][x]['email'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./users/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let roles
		if (auth.user.id == 1) {
			roles = await Role.query().select('id', 'role_title').orderBy('role_title', 'ASC').fetch()
		} else {
			roles = await Role.query().select('id', 'role_title').where('role_slug', '!=', 'superadmin').orderBy('role_title', 'ASC').fetch()
		}
		
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('users.create', {
			meta: meta,
			roles: roles.toJSON()
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let { fullname, username, email, password, user_type, user_role } = request.only(['fullname', 'username', 'email', 'password','user_type','user_role'])
		
		let formData = {
			username: username,
			email: email,
			fullname: fullname,
			password: password,
			user_type: user_type,
			user_role: user_role,
			social_token: '',
			activation_key: await Hash.make(password),
			block: 'Y',
			forget_key: null,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			username: 'required|unique:users,username',
			email: 'required|email|unique:users,email',
			password: 'required',
			user_type: 'required',
			user_role: 'required',
		}
		
		const validation = await validateAll(formData, rules)	
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password'])
			return response.redirect('back')
		} else {
			if (auth.user.id == '1') {
				let newuser = await User.create(formData)
				await RoleUser.create({
					role_id: user_role,
					user_id: newuser.id,
					created_by: auth.user.id,
					updated_by: auth.user.id
				})
				session.flash({ notification: 'User added', status: 'aquamarine' })
				return response.redirect('/users')
			} else {
				session.flash({ notification: 'Not Authorization', status: 'danger' })
				return response.redirect('/users')
			}
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let user
		if (auth.user.id == '1') {
			user = await User.query().where('users.id', params.id).first()
		} else {
			if (auth.user.id == params.id) {
				user = await User.query().where('users.id', params.id).first()
			} else {
				user = false
			}
		}
		
		if (user) {
			let roles
			if (auth.user.id == 1) {
				roles = await Role.query().select('id', 'role_title').orderBy('role_title', 'ASC').fetch()
			} else {
				roles = await Role.query().select('id', 'role_title').where('role_slug', '!=', 'superadmin').orderBy('role_title', 'ASC').fetch()
			}
			
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('users.edit', {
				meta: meta,
				user: user,
				roles: roles.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'User not found', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { username, email, password,user_type, user_role } = request.only(['username', 'email', 'password','user_type','user_role'])

		let user
		if (auth.user.id == '1') {
			user = await User.find(params.id)
		} else {
			if (auth.user.id == params.id) {
				user = await User.find(params.id)
			} else {
				user = false
			}
		}
		
		const userData = {
			username: username,
			email: email,
			user_type: user_type,
			user_role: user_role
		}
		
		let rules = {
			username: `required|unique:users,username,id,${user.id}`,
			email: `required|unique:users,email,id,${user.id}`,
			user_type: 'required',
			user_role: 'required'
		}

		const validation = await validateAll(userData, rules)	
		if (validation.fails()) {
			session.withErrors(validation.messages()).flashExcept(['password'])
			return response.redirect('back')
		} else {
			if (user) {
				if (password == '' || password == null) {
					await User.query().where('id', params.id).update({
						username: username,
						email: email,
						user_type: user_type,
						user_role: user_role,
						updated_by: auth.user.id
					})
				} else {
					await User.query().where('id', params.id).update({
						username: username,
						email: email,
						password: await Hash.make(password),
						user_type: user_type,
						user_role: user_role,						
						updated_by: auth.user.id
					})
				}
				await RoleUser.query().where('id', params.id).update({
					role_id: user_role,
					user_id: params.id,
					updated_by: auth.user.id
				})
				session.flash({ notification: 'User updated', status: 'aquamarine' })
				return response.redirect('/users')
			} else {
				session.flash({ notification: 'User not found', status: 'danger' })
				return response.redirect('/users')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.id != auth.user.id) {
			let user
			if (auth.user.id == '1') {
				user = await User.find(formData.id)
			} else {
				if (auth.user.id == formData.id) {
					user = await User.find(formData.id)
				} else {
					user = false
				}
			}
			if (user){      
				try {
					await user.delete()
					session.flash({ notification: 'User success deleted', status: 'aquamarine' })
					return response.redirect('/users')
				} catch (e) {
					session.flash({ notification: 'User cannot be delete', status: 'danger' })
					return response.redirect('/users')
				}
			} else {
				session.flash({ notification: 'User cannot be delete', status: 'danger' })
				return response.redirect('/users')
			}
		} else {
			session.flash({ notification: 'User cannot be delete', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('User', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				if (formData.item[i] != auth.user.id) {
					let user
					if (auth.user.id == '1') {
						user = await User.find(formData.item[i])
						try {
							await user.delete()
						} catch (e) {}
					} else {
						if (auth.user.id == formData.id) {
							user = await User.find(formData.item[i])
							try {
								await user.delete()
							} catch (e) {}
						} else {
							user = false
						}
					}
				}
			}
			session.flash({ notification: 'User success deleted', status: 'aquamarine' })
			return response.redirect('/users')
		} else {
			session.flash({ notification: 'User cannot be deleted', status: 'danger' })
			return response.redirect('/users')
		}
	}
	
	async getUser({request, response, auth, view, session}) {
		let req = request.get()
		let users
		if (req.phrase != 'undefined') {
			users = await User.query().select('users.id', 'users.fullname', 'users.email').where('users.fullname', 'LIKE', '%' + req.phrase + '%').orWhere('users.email', 'LIKE', '%' + req.phrase + '%').limit(20).orderBy('users.id', 'DESC').fetch()
		} else {
			users = await User.query().select('users.id', 'users.fullname', 'users.email').limit(20).orderBy('users.id', 'DESC').fetch()
		}
		let user = users.toJSON()
		
		let data = []
		for(let i in user){
			data.push({
				id: user[i]['id'],
				text: user[i]['fullname'] + ' (' + user[i]['email'] + ')'
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = UserController