'use strict'

const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const Logger = use('Logger')
const Env = use('Env')
const moment = use('moment')
const minifyHTML = require('./Helper/MinifyHTML.js')

class HomeController {
	async getDashboard({ request, response, auth, view }) {
		let settings = (await Setting.query().select('id', 'code', 'key', 'value', 'serialized').whereIn('id', [1,2,3,4,5,21]).fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['web_name'],
			author: settings[4]['web_owner'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[5]['value']
		}
		
		let template = view.render('dashboard', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
}

module.exports = HomeController