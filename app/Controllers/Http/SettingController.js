'use strict'

const Setting = use('App/Models/Setting')
const Country = use('App/Models/Country')
const Province = use('App/Models/Province')
const Language = use('App/Models/Language')
const Currency = use('App/Models/Currency')
const Uom = use('App/Models/Uom')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class SettingController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Setting', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().fetch()).toJSON()
		let countrys = await Country.query().fetch()
		let provinces = await Province.query().fetch()
		let languages = await Language.query().fetch()
		let currencys = await Currency.query().fetch()
		let uoms = await Uom.query().fetch()
		
		let settings31s = settings[31]['value']
		let settings31 = settings31s.replace(/,/g, '\r\n')
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
			
		let template = view.render('setting.index', {
			meta: meta,
			settings: settings,
			countrys: countrys.toJSON(),
			provinces: provinces.toJSON(),
			languages: languages.toJSON(),
			currencys: currencys.toJSON(),
			uoms: uoms.toJSON(),
			settings31: settings31
		})
		
		return await minifyHTML.minify(template)
	}
	
	async updateall({request, response, auth, view, session}) {
		const formData = request.all()
		
		await Setting.query().where('id', '1').update({ value: formData.meta_title })
		await Setting.query().where('id', '2').update({ value: formData.meta_description })
		await Setting.query().where('id', '3').update({ value: formData.meta_keyword })
		await Setting.query().where('id', '4').update({ value: formData.web_name })
		await Setting.query().where('id', '5').update({ value: formData.web_owner })
		await Setting.query().where('id', '6').update({ value: formData.address })
		await Setting.query().where('id', '7').update({ value: formData.geocode })
		await Setting.query().where('id', '8').update({ value: formData.email })
		await Setting.query().where('id', '9').update({ value: formData.telephone })
		await Setting.query().where('id', '10').update({ value: formData.fax })
		await Setting.query().where('id', '11').update({ value: formData.image })
		await Setting.query().where('id', '12').update({ value: formData.openning_times })
		await Setting.query().where('id', '13').update({ value: formData.country })
		await Setting.query().where('id', '14').update({ value: formData.province })
		await Setting.query().where('id', '15').update({ value: formData.language })
		await Setting.query().where('id', '16').update({ value: formData.administration_language })
		await Setting.query().where('id', '17').update({ value: formData.currency })
		await Setting.query().where('id', '18').update({ value: formData.auto_update_currency })
		await Setting.query().where('id', '19').update({ value: formData.length_class })
		await Setting.query().where('id', '20').update({ value: formData.weight_class })
		await Setting.query().where('id', '21').update({ value: formData.web_logo })
		await Setting.query().where('id', '22').update({ value: formData.icon })
		await Setting.query().where('id', '23').update({ value: formData.mail_engine })
		await Setting.query().where('id', '24').update({ value: formData.mail_parameters })
		await Setting.query().where('id', '25').update({ value: formData.smtp_hostname })
		await Setting.query().where('id', '26').update({ value: formData.smtp_username })
		await Setting.query().where('id', '27').update({ value: formData.smtp_password })
		await Setting.query().where('id', '28').update({ value: formData.smtp_port })
		await Setting.query().where('id', '29').update({ value: formData.smtp_timeout })
		await Setting.query().where('id', '30').update({ value: formData.maintenance_mode })
		await Setting.query().where('id', '31').update({ value: formData.use_seo_url })
		let robots = formData.robot
		let robot = robots.replace(/(\r\n|\n|\r)/gm, ',')
		await Setting.query().where('id', '32').update({ value: robot })
		await Setting.query().where('id', '33').update({ value: formData.output_compression_level })
		await Setting.query().where('id', '34').update({ value: formData.allow_forgotten_password })
		await Setting.query().where('id', '35').update({ value: formData.encryption_key })

		session.flash({ notification: 'Setting updated', status: 'aquamarine' })
		return response.redirect('/setting')
	}
}

module.exports = SettingController