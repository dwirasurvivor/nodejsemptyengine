'use strict'

const District = use('App/Models/District')
const Regency = use('App/Models/Regency')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class DistrictController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('District', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('district.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'districts',
			sSelectSql: ['id', 'regency_id', '(select regency_name from regencies where regency_id = id) as regency_name', 'district_name', 'district_iso', 'district_hasc', 'district_fips'],
			aSearchColumns: ['district_name', 'district_iso', 'district_hasc', 'district_fips']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['regency_name'],
				select[0][x]['district_name'],
				select[0][x]['district_iso'],
				select[0][x]['district_hasc'],
				select[0][x]['district_fips'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./district/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let regencies = await Regency.all()
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('district.create', {
			meta: meta,
			regencies: regencies.toJSON()
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { regency_id, district_name, district_iso, district_hasc, district_fips } = request.only(['regency_id', 'district_name', 'district_iso', 'district_hasc', 'district_fips'])
		
		let formData = {
			regency_id: regency_id,
			district_name: district_name,
			district_iso: district_iso,
			district_hasc: district_hasc,
			district_fips: district_fips,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			regency_id: 'required',
			district_name: 'required',
			district_iso: 'required',
			district_hasc: 'required',
			district_fips: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await District.create(formData)
			session.flash({ notification: 'District added', status: 'aquamarine' })
			return response.redirect('/district')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let district = await District.find(params.id)
		let regencies = await Regency.all()

		if (district) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('district.edit', {
				meta: meta,
				district: district,
				regencies:regencies.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'District not found', status: 'danger' })
			return response.redirect('/district')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { regency_id, district_name, district_iso, district_hasc, district_fips } = request.only(['regency_id', 'district_name', 'district_iso', 'district_hasc', 'district_fips'])
		
		let district = await District.find(params.id)
		
		let formData = {
			regency_id: regency_id,
			district_name: district_name,
			district_iso: district_iso,
			district_hasc: district_hasc,
			district_fips: district_fips,
			updated_by: auth.user.id
		}
		
		let rules = {
			regency_id: 'required',
			district_name: 'required',
			district_iso: 'required',
			district_hasc: 'required',
			district_fips: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (district) {
				await District.query().where('id', params.id).update(formData)
				session.flash({ notification: 'District updated', status: 'aquamarine' })
				return response.redirect('/district')
			} else {
				session.flash({ notification: 'District not found', status: 'danger' })
				return response.redirect('/district')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let district = await District.find(formData.id)
		if (district){
			try {
				await district.delete()
				session.flash({ notification: 'District success deleted', status: 'aquamarine' })
				return response.redirect('/district')
			} catch (e) {
				session.flash({ notification: 'District cannot be delete', status: 'danger' })
				return response.redirect('/district')
			}
		} else {
			session.flash({ notification: 'District cannot be delete', status: 'danger' })
			return response.redirect('/district')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let district = await District.find(formData.item[i])
				try {
					await district.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'District success deleted', status: 'aquamarine' })
			return response.redirect('/district')
		} else {
			session.flash({ notification: 'District cannot be deleted', status: 'danger' })
			return response.redirect('/district')
		}
	}

	async getbyregency({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('District', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let district = await District.query().select('id','district_name as name').where('regency_id',params.id).orderBy('district_name', 'asc');
		return district
	}
	
	async getDistrict({request, response, auth, view, session}) {
		let req = request.get()
		let districts
		if (req.phrase != 'undefined') {
			districts = await District.query().select('id', 'district_name').where('district_name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			districts = await District.query().select('id', 'district_name').limit(20).fetch()
		}
		let district = districts.toJSON()
		
		let data = []
		for(let i in district){
			data.push({
				id: district[i]['id'],
				text: district[i]['district_name']
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = DistrictController