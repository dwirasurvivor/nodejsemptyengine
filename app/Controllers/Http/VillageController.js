'use strict'

const Village = use('App/Models/Village')
const District = use ('App/Models/District')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class VillageController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Village', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('village.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'villages',
			sSelectSql: ['id', 'district_id', '(select district_name from districts where district_id = id) as district_name', 'village_name', 'postcode'],
			aSearchColumns: ['village_name', 'postcode']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['district_name'],
				select[0][x]['village_name'],
				select[0][x]['postcode'],
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./village/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let districts = await District.all()
		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('village.create', {
			meta: meta,
			districts: districts.toJSON()
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { district_id, village_name, postcode } = request.only(['district_id', 'village_name', 'postcode'])
		
		let formData = {
			district_id: district_id,
			village_name: village_name,
			postcode: postcode,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			district_id: 'required',
			village_name: 'required',
			postcode: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Village.create(formData)
			session.flash({ notification: 'Village added', status: 'aquamarine' })
			return response.redirect('/village')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let village = await Village.find(params.id)
		let districts = await District.all()

		if (village) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('village.edit', {
				meta: meta,
				village: village,
				districts: districts.toJSON()
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Village not found', status: 'danger' })
			return response.redirect('/village')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { district_id, village_name, postcode } = request.only(['district_id', 'village_name', 'postcode'])
		
		let village = await Village.find(params.id)
		
		let formData = {
			district_id: district_id,
			village_name: village_name,
			postcode: postcode,
			updated_by: auth.user.id
		}
		
		let rules = {
			district_id: 'required',
			village_name: 'required',
			postcode: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (village) {
				await Village.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Village updated', status: 'aquamarine' })
				return response.redirect('/village')
			} else {
				session.flash({ notification: 'Village not found', status: 'danger' })
				return response.redirect('/village')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let village = await Village.find(formData.id)
		if (village){
			try {
				await village.delete()
				session.flash({ notification: 'Village success deleted', status: 'aquamarine' })
				return response.redirect('/village')
			} catch (e) {
				session.flash({ notification: 'Village cannot be delete', status: 'danger' })
				return response.redirect('/village')
			}
		} else {
			session.flash({ notification: 'Village cannot be delete', status: 'danger' })
			return response.redirect('/village')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let village = await Village.find(formData.item[i])
				try {
					await village.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Village success deleted', status: 'aquamarine' })
			return response.redirect('/village')
		} else {
			session.flash({ notification: 'Village cannot be deleted', status: 'danger' })
			return response.redirect('/village')
		}
	}

	async getbydistrict({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Village', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let village = await Village.query().select('id','village_name as name','postcode as kodepos').where('district_id',params.id).orderBy('village_name', 'asc');		
		return village
	}

	async getPostcode({request, response, auth, view, session}) {
		let req = request.get()
		let postcodes
		if (req.phrase != 'undefined') {
			postcodes = await Village.query().select('id', 'postcode').where('postcode', 'LIKE', '%' + req.phrase + '%').groupBy('postcode').limit(20).fetch()
		} else {
			postcodes = await Village.query().select('id', 'postcode').groupBy('postcode').limit(20).fetch()
		}
		let postcode = postcodes.toJSON()
		
		let data = []
		for(let i in postcode){
			data.push({
				id: postcode[i]['id'],
				text: postcode[i]['postcode'].toString()
			})
		}
		
		return JSON.stringify({ results: data })
	}
}

module.exports = VillageController