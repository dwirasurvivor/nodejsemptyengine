'use strict'

const Language = use('App/Models/Language')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class LanguageController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Language', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('language.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'languages',
			sSelectSql: ['id', 'language_code', 'language_name', 'active'],
			aSearchColumns: ['language_code', 'language_name']
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))
		
		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = await Database.raw(queries.recordsFiltered)
		
		let fdata = []
		let no = 0
		for(let x in select[0]) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' /><input type='hidden' class='deldata' name='item[]' value='"+ select[0][x]['id'] +"' disabled /></div>\n",
				select[0][x]['id'],
				select[0][x]['language_code'],
				select[0][x]['language_name'],
				select[0][x]['active'] == 1 ? 'Aktif' : 'Tidak Aktif',
				"<div class='text-center'><div class='btn-group btn-group-sm'><a href='./language/"+ select[0][x]['id'] +"/edit' class='btn btn-sm btn-primary' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Edit'><i class='fa fa-pencil'></i></a><a class='btn btn-sm btn-danger alertdel' id='"+ select[0][x]['id'] +"' data-toggle='tooltip' title='Delete'><i class='fa fa-times'></i></a></div></div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			recordsFiltered: (queries.recordsFiltered) ? JSON.stringify(recordsFiltered[0][0]['COUNT(*)']) : JSON.stringify(recordsTotal[0][0]['COUNT(*)']),
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			image: Env.get('BASE_URL') + '/' + settings[20]['value']
		}
		
		let template = view.render('language.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { language_code, language_name, active } = request.only(['language_code', 'language_name', 'active'])
		
		let formData = {
			language_code: language_code,
			language_name: language_name,
			active: active,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}
		
		const rules = {
			language_code: 'required',
			language_name: 'required',
			active: 'required',
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			await Language.create(formData)
			session.flash({ notification: 'Language added', status: 'aquamarine' })
			return response.redirect('/language')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let language = await Language.find(params.id)

		if (language) {
			let settings = (await Setting.query().fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				image: Env.get('BASE_URL') + '/' + settings[20]['value']
			}
			
			let template = view.render('language.edit', {
				meta: meta,
				language: language
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Language not found', status: 'danger' })
			return response.redirect('/language')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const { language_code, language_name, active } = request.only(['language_code', 'language_name', 'active'])
		
		let language = await Language.find(params.id)
		
		let formData = {
			language_code: language_code,
			language_name: language_name,
			active: active,
			updated_by: auth.user.id
		}
		
		let rules = {
			language_code: 'required',
			language_name: 'required',
			active: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (language) {
				await Language.query().where('id', params.id).update(formData)
				session.flash({ notification: 'Language updated', status: 'aquamarine' })
				return response.redirect('/language')
			} else {
				session.flash({ notification: 'Language not found', status: 'danger' })
				return response.redirect('/language')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let language = await Language.find(formData.id)
		if (language){
			try {
				await language.delete()
				session.flash({ notification: 'Language success deleted', status: 'aquamarine' })
				return response.redirect('/language')
			} catch (e) {
				session.flash({ notification: 'Language cannot be delete', status: 'danger' })
				return response.redirect('/language')
			}
		} else {
			session.flash({ notification: 'Language cannot be delete', status: 'danger' })
			return response.redirect('/language')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Language', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let language = await Language.find(formData.item[i])
				try {
					await language.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Language success deleted', status: 'aquamarine' })
			return response.redirect('/language')
		} else {
			session.flash({ notification: 'Language cannot be deleted', status: 'danger' })
			return response.redirect('/language')
		}
	}
}

module.exports = LanguageController