'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {import('@adonisjs/framework/src/Route/Manager'} */
const Route = use('Route')

// Router for Frontend
Route.get('/', 'AccountController.getIndex')
Route.get('login', 'AccountController.getLogin')
Route.post('login', 'AccountController.postLogin')
Route.get('logout', 'AccountController.getLogout')
Route.get('error', 'ErrorController.index')

// Router for Backend
Route.group(() => {
	Route.get('home', 'HomeController.getDashboard')
	
	Route.get('filemanager', 'FilemanagerController.index')
	Route.get('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/getdata', 'FilemanagerController.getData').formats(['json'])
	Route.post('filemanager/upload', 'FilemanagerController.upload').formats(['json'])
	Route.post('filemanager/delete', 'FilemanagerController.delete').formats(['json'])
	
	Route.get('users/getuser', 'UserController.getUser').formats(['json'])
	Route.post('users/datatable', 'UserController.datatable')
	Route.post('users/delete', 'UserController.delete')
	Route.post('users/multidelete', 'UserController.multidelete')
	Route.resource('users', 'UserController')
	
	Route.post('role/datatable', 'RoleController.datatable')
	Route.post('role/delete', 'RoleController.delete')
	Route.post('role/multidelete', 'RoleController.multidelete')
	Route.resource('role', 'RoleController')
	
	Route.post('roles-users/datatable', 'RolesUsersController.datatable')
	Route.post('roles-users/delete', 'RolesUsersController.delete')
	Route.post('roles-users/multidelete', 'RolesUsersController.multidelete')
	Route.resource('roles-users', 'RolesUsersController')
	
	Route.post('setting/updateall', 'SettingController.updateall')
	Route.resource('setting', 'SettingController')
	
	Route.post('apiservice/datatable', 'ApiServiceController.datatable')
	Route.post('apiservice/delete', 'ApiServiceController.delete')
	Route.post('apiservice/multidelete', 'ApiServiceController.multidelete')
	Route.resource('apiservice', 'ApiServiceController')
	
	Route.post('systemlog/datatable', 'SystemLogController.datatable')
	Route.post('systemlog/delete', 'SystemLogController.delete')
	Route.post('systemlog/multidelete', 'SystemLogController.multidelete')
	Route.resource('systemlog', 'SystemLogController')
	
	Route.post('language/datatable', 'LanguageController.datatable')
	Route.post('language/delete', 'LanguageController.delete')
	Route.post('language/multidelete', 'LanguageController.multidelete')
	Route.resource('language', 'LanguageController')
	
	Route.get('country/getcountry', 'CountryController.getCountry').formats(['json'])
	Route.get('country/getall', 'CountryController.getCountryList')
	Route.post('country/datatable', 'CountryController.datatable')
	Route.post('country/delete', 'CountryController.delete')
	Route.post('country/multidelete', 'CountryController.multidelete')
	Route.resource('country', 'CountryController')

	Route.get('province/getprovince', 'ProvinceController.getProvince').formats(['json'])
	Route.get('province/getbycountry/:id', 'ProvinceController.getByCountry')
	Route.post('province/datatable', 'ProvinceController.datatable')
	Route.post('province/delete', 'ProvinceController.delete')
	Route.post('province/multidelete', 'ProvinceController.multidelete')
	Route.resource('province', 'ProvinceController')
	
	Route.get('regency/getregency', 'RegencyController.getRegency').formats(['json'])
	Route.get('regency/getbyprovince/:id', 'RegencyController.getbyprovince')
	Route.post('regency/datatable', 'RegencyController.datatable')
	Route.post('regency/delete', 'RegencyController.delete')
	Route.post('regency/multidelete', 'RegencyController.multidelete')
	Route.resource('regency', 'RegencyController')

	Route.get('district/getdistrict', 'DistrictController.getDistrict').formats(['json'])
	Route.get('district/getbyregency/:id', 'DistrictController.getbyregency')
	Route.post('district/datatable', 'DistrictController.datatable')
	Route.post('district/delete', 'DistrictController.delete')
	Route.post('district/multidelete', 'DistrictController.multidelete')
	Route.resource('district', 'DistrictController')

	Route.get('village/getpostcode', 'VillageController.getPostcode').formats(['json'])
	Route.get('village/getbydistrict/:id', 'VillageController.getbydistrict')
	Route.post('village/datatable', 'VillageController.datatable')
	Route.post('village/delete', 'VillageController.delete')
	Route.post('village/multidelete', 'VillageController.multidelete')
	Route.resource('village', 'VillageController')
	
	Route.get('uom/getcurrency', 'UomController.getCurrency').formats(['json'])
	Route.post('currency/datatable', 'CurrencyController.datatable')
	Route.post('currency/delete', 'CurrencyController.delete')
	Route.post('currency/multidelete', 'CurrencyController.multidelete')
	Route.resource('currency', 'CurrencyController')
	
	Route.get('uom/getuom', 'UomController.getUom').formats(['json'])
	Route.post('uom/datatable', 'UomController.datatable')
	Route.post('uom/delete', 'UomController.delete')
	Route.post('uom/multidelete', 'UomController.multidelete')
	Route.resource('uom', 'UomController')

// --------------------------

	Route.get('generator', 'GeneratorController.index')
	Route.post('generator/generated', 'GeneratorController.generated')
})
.middleware('auth')
.middleware('roles')
.middleware('globalparam')

// Router for API
require('./api.js')