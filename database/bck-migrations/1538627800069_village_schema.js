'use strict'

const Schema = use('Schema')

class VillagesSchema extends Schema {
	up () {
		this.create('villages', (table) => {
			table.increments()
			table.integer('district_id')
			table.string('village_name')
			table.integer('postcode')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('villages')
	}
}

module.exports = VillagesSchema