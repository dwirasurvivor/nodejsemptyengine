'use strict'

const Schema = use('Schema')

class SettingSchema extends Schema {
	up () {
		this.create('settings', (table) => {
			table.increments()
			table.string('code', 255)
			table.string('key', 255)
			table.text('value')
			table.boolean('serialized').defaultTo(0)
			table.integer('created_by', 10).defaultTo(1)
			table.integer('updated_by', 10).defaultTo(1)
			table.timestamps()
		})
	}

	down () {
		this.drop('settings')
	}
}

module.exports = SettingSchema

