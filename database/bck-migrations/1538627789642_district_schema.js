'use strict'

const Schema = use('Schema')

class DistrictsSchema extends Schema {
	up () {
		this.create('districts', (table) => {
			table.increments()
			table.integer('regency_id')
			table.string('district_name')
			table.string('district_iso')
			table.string('district_hasc')
			table.integer('district_fips')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('districts')
	}
}

module.exports = DistrictsSchema