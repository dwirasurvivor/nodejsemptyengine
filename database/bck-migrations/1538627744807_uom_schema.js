'use strict'

const Schema = use('Schema')

class UomsSchema extends Schema {
	up () {
		this.create('uoms', (table) => {
			table.increments()
			table.string('uom_code')
			table.string('uom_name')
			table.string('uom_description')
			table.integer('uom_sorting')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('uoms')
	}
}

module.exports = UomsSchema