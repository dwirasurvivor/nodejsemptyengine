'use strict'

const Schema = use('Schema')

class SystemLogsSchema extends Schema {
	up () {
		this.create('system_logs', (table) => {
			table.increments()
			table.integer('user_id')
			table.string('access')
			table.string('ip')
			table.string('user_agent')
			table.text('browser')
			table.text('cpu')
			table.text('device')
			table.text('engine')
			table.text('os')
			table.string('url')
			table.string('method')
			table.string('param')
			table.text('body')
			table.text('response')
			table.integer('created_by', 10).defaultTo(1)
			table.integer('updated_by', 10).defaultTo(1)
			table.timestamps()
		})
		this.raw('ALTER TABLE `system_logs` ADD INDEX `user_id` (`user_id`)')
	}

	down () {
		this.drop('system_logs')
	}
}

module.exports = SystemLogsSchema