'use strict'

const Schema = use('Schema')

class CountriesSchema extends Schema {
	up () {
		this.create('countries', (table) => {
			table.increments()
			table.string('country_code')
			table.string('country_name')
			table.string('iso_country_code')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('countries')
	}
}

module.exports = CountriesSchema