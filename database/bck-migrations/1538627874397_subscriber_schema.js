'use strict'

const Schema = use('Schema')

class SubscriberSchema extends Schema {
	up () {
		this.create('subscribers', (table) => {
			table.increments()
			table.string('email')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('subscribers')
	}
}

module.exports = SubscriberSchema