'use strict'

const Schema = use('Schema')

class CurrenciesSchema extends Schema {
	up () {
		this.create('currencies', (table) => {
			table.increments()
			table.string('currency_code')
			table.string('currency_name')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('currencies')
	}
}

module.exports = CurrenciesSchema