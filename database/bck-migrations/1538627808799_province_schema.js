'use strict'

const Schema = use('Schema')

class ProvincesSchema extends Schema {
	up () {
		this.create('provinces', (table) => {
			table.increments()
			table.string('province_code')
			table.string('province_name')
			table.integer('country_id')
			table.string('province_iso')
			table.string('province_hasc')
			table.integer('province_fips')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('provinces')
	}
}

module.exports = ProvincesSchema