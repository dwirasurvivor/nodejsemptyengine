'use strict'

const Schema = use('Schema')

class RegenciesSchema extends Schema {
	up () {
		this.create('regencies', (table) => {
			table.increments()
			table.integer('province_id')
			table.string('regency_name')
			table.string('regency_iso')
			table.string('regency_hasc')
			table.integer('regency_fips')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('regencies')
	}
}

module.exports = RegenciesSchema