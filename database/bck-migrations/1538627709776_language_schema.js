'use strict'

const Schema = use('Schema')

class LanguagesSchema extends Schema {
	up () {
		this.create('languages', (table) => {
			table.increments()
			table.string('language_code')
			table.string('language_name')
			table.integer('active')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('languages')
	}
}

module.exports = LanguagesSchema